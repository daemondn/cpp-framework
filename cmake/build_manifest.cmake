# Copyright (c) 2013, 2014 Corvusoft

set( MANIFEST
     ${SOURCE_DIR}/map.cpp
     ${SOURCE_DIR}/detail/map_impl.cpp
     ${SOURCE_DIR}/uri.cpp
     ${SOURCE_DIR}/detail/uri_impl.cpp
     ${SOURCE_DIR}/date.cpp
     ${SOURCE_DIR}/detail/date_impl.cpp
     ${SOURCE_DIR}/regex.cpp
     ${SOURCE_DIR}/detail/regex_impl.cpp
     ${SOURCE_DIR}/run_id.cpp
     ${SOURCE_DIR}/detail/run_id_impl.cpp
     ${SOURCE_DIR}/string.cpp
     ${SOURCE_DIR}/detail/string_impl.cpp
     ${SOURCE_DIR}/vector.cpp
     ${SOURCE_DIR}/detail/vector_impl.cpp
     ${SOURCE_DIR}/istream.cpp
     ${SOURCE_DIR}/detail/istream_impl.cpp
     ${SOURCE_DIR}/checksum.cpp
     ${SOURCE_DIR}/detail/checksum_impl.cpp
     ${SOURCE_DIR}/unique_id.cpp
     ${SOURCE_DIR}/detail/unique_id_impl.cpp
     ${SOURCE_DIR}/password.cpp
     ${SOURCE_DIR}/detail/password_impl.cpp
     ${SOURCE_DIR}/username.cpp
     ${SOURCE_DIR}/detail/username_impl.cpp
     ${SOURCE_DIR}/http.cpp
     ${SOURCE_DIR}/detail/http_impl.cpp
     ${SOURCE_DIR}/hexidecimal.cpp
     ${SOURCE_DIR}/detail/hexidecimal_impl.cpp
)
